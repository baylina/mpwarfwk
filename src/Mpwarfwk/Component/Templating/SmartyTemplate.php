<?php

namespace Mpwarfwk\Component\Templating;

use \Smarty;

class SmartyTemplate implements Templating {

    public function __construct (){

        $this->view = new Smarty();
    }

    public function render($template,$variables = null){
        return $this->view->fetch($template);
    }

    public function assignVars($variables){

        foreach($variables as $key => $value) {

            $this->view->assign($key,$value);
        }
    }
}