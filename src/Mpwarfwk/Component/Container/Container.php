<?php

namespace Mpwarfwk\Component\Container;
use \ReflectionClass;

class Container {


    public function __construct() {


    }

    public function get($service)
    {
        include '../src/Config/services.php';

        $arguments = array();

        // If exists a service
        if (!empty($config[$service]))
        {
            // If there are arguments
            if (!empty($config[$service]['arguments']))
            {
                // Instantiate all arguments
                foreach ($config[$service]['arguments'] as $argument) {

                    $arguments[] = new $argument();
                }
            }

            $reflection = new ReflectionClass($config[$service]['controller']);
            return $reflection->newInstanceArgs($arguments);
        }
        throw new \InvalidArgumentException();
    }



}


