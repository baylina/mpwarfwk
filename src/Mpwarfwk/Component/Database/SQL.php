<?php

namespace Mpwarfwk\Component\Database;
use \PDO;
use \PDOException;

class SQL extends \PDO
{

    public function __construct($DB_DRIVER, $DB_HOST, $DB_NAME, $DB_USER, $DB_PASS)
    {
        $options = array(
            \PDO::ATTR_PERSISTENT => true,
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
        );
        try {
            parent::__construct($DB_DRIVER . ':host=' . $DB_HOST . ';dbname=' . $DB_NAME, $DB_USER, $DB_PASS, $options);
        } catch (\PDOException $e) {
            die($e->getMessage());
        }
    }


    public function selectWithQueryAndData($sql, $data = NULL) {

        $sth = $this->prepare($sql);
        if($data != NULL){
            foreach ($data as $key => $currentValue) {
                $sth->bindValue(":$key", $currentValue);
            }
        }
        $sth->execute();
        return $sth->fetchAll();
    }

    // Select all information
    public function selectAllWithQuery($sql)
    {

        $fetchMode = \PDO::FETCH_ASSOC;
        $sth = $this->prepare($sql);
        $sth->execute();

        return $sth->fetchAll($fetchMode);
    }


    // Insert with data
    public function insertIntoTableWithData($table, $data)
    {

        ksort($data);
        $fieldNames = '`'. implode("`,`", array_keys($data)). '`';
        $fieldValues = ':' . implode(', :', array_keys($data));
        $sth = $this->prepare("INSERT INTO $table ($fieldNames) VALUES ($fieldValues)");

        foreach ($data as $key => $value) {
            $sth->bindValue(":$key", $value);
        }

        $sth->execute();

     }

    public function deleteWithQuery($sql) {

        $sth = $this->prepare($sql);
        return $sth->execute();

    }

    public function updateWithQueryAndData($query, $data) {

        $sth = $this->prepare($query);
        foreach ($data as $key => $actualValue) {
            $sth->bindValue(":$key", $actualValue);
        }
        return $sth->execute();
    }



}



