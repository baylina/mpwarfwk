<?php

namespace Mpwarfwk\Component;

use Mpwarfwk\Component\Request\Request;
use Mpwarfwk\Component\Session\Session;
use Mpwarfwk\Component\Routing\Routing;
use Mpwarfwk\Component\Routing\Route;


class Bootstrap
{
    public $production = false;

    public function __construct()
    {

    }

    // Create basic components: Session, Request, Routing and execute Controller
    public function execute()
    {

        $session = new Session();

        $request = new Request($session);

        $routing = new Routing($request);

        $route = $routing->getRoute();

        $response = $this->executeControllerWithRouteAndRequest($route,$request);

        return $response;

    }

    // Return response
    public function executeControllerWithRouteAndRequest(Route $route, Request $request) {


        $controller_class = $route->getRouteClass();

        return call_user_func_array(
            array(
                new $controller_class(),
                $route->getRouteAction()
            ),
            array($request)
        );

    }
}
